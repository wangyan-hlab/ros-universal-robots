import cv2
import numpy as np
import PIL
from collections import deque
from frame_classifier import FrameClassifier
from pathlib import Path

def cv2pil(image):
    ''' OpenCV -> PIL '''
    new_image = image.copy()
    if new_image.ndim == 2:  # モノクロ
        pass
    elif new_image.shape[2] == 3:  # カラー
        new_image = cv2.cvtColor(new_image, cv2.COLOR_BGR2RGB)
    elif new_image.shape[2] == 4:  # 透過
        new_image = cv2.cvtColor(new_image, cv2.COLOR_BGRA2RGBA)
    new_image = PIL.Image.fromarray(new_image)
    return new_image

class VideoClassifier():
    def __init__(self, stage="glue"):
        # How many batches are needed to cover the pipe surface
        self.batch_qty = 6
        # How many frames per batch
        self.batch_size = 20
        # Bbox for cropping the images
        self.bbox = None
        
        # Load trained model (either for predict_glue or to predict_smooth)
        glue_prediction_model = "/root/dev/image_classification/training2.1_v3_mobilev3Large_224-2021-030-2-10-19-04.h5"
        smooth_prediction_model = "/root/dev/image_classification/training2.2_v3_mobilev3Large_224-2021-03-03-15-25-41.h5"
        self.prediction_stage = stage
        self.glue_model = FrameClassifier(model_path=glue_prediction_model)
        self.smooth_model = FrameClassifier(model_path=smooth_prediction_model)

        # Determine the label of interest
        self.target_label = 1.0
        self.tolerance = 0.6

        self.image_queue = deque(maxlen=self.batch_size)
        self.batch_queue = deque(maxlen=self.batch_qty)

        self.counter = 0
        self.counter_lbl = np.arange(self.batch_qty, dtype='i') + 1

    def image_classification(self, frame):
        if not isinstance(frame, np.ndarray):
            return

        img = cv2pil(frame)

        # crop image if requested
        if self.bbox is not None:
            # expected format bbox [min x, min y, max x, max y]
            img = img.crop(tuple(self.bbox))
       
        model = self.glue_model if self.prediction_stage == "glue" else self.smooth_model

        # One batch
        if len(self.image_queue) == self.image_queue.maxlen:
            glue_labels = {0:"NG", 1:"G"}
            smooth_labels = {0:"NSM", 1:"SM"}
            labels = glue_labels if self.prediction_stage == "glue" else smooth_labels

            batch_avg = np.average(np.array(self.image_queue)) 
            self.batch_queue.append(1.0 if batch_avg > self.tolerance else 0.0)

            pipe_prediction = np.sum(self.batch_queue)/float(self.batch_qty) * 100 \
                                if self.target_label == 1.0 \
                                else 1.0 - (np.sum(self.batch_queue)/float(self.batch_qty)) * 100
            print("batch predictions ", [labels.get(pred) for pred in self.batch_queue], " task ", pipe_prediction,"%")
            

            # reset batch
            self.image_queue = deque(maxlen=self.batch_size)
            res = str([ str(c)+'-'+labels.get(pred) for pred,c in zip(self.batch_queue, self.counter_lbl[:len(self.batch_queue)])]) + \
                   " task " + str(pipe_prediction) + "%"

            if len(self.batch_queue) == self.batch_queue.maxlen:
                self.counter_lbl = np.roll(self.counter_lbl, -1)
            
            return res, pipe_prediction
        
        if len(self.image_queue) < self.image_queue.maxlen:
            image = img.resize((224, 224), PIL.Image.NEAREST)
            self.image_queue.append(model.predict([image])[0])

        self.counter += 1
        if self.counter == self.batch_qty*self.batch_size:
            self.counter = 0

        return None, 0.0


def main():
    filename = Path('/root/dev/results/episode4_smooth.mp4')
    phase = "glue" if "glue" in filename.name else "smooth"
    cap = cv2.VideoCapture(str(filename))
    frame_width = int(cap.get(3))
    frame_height = int(cap.get(4))
    
    outfile = str(filename.parent) + '/' + filename.stem + "_pred" + filename.suffix
    out = cv2.VideoWriter(outfile, cv2.VideoWriter_fourcc(*'MP4V'), 30, (frame_width,frame_height))

    framespersecond= int(cap.get(cv2.CAP_PROP_FPS))
    print("FPS", framespersecond)

    vc = VideoClassifier(phase)

    font                   = cv2.FONT_HERSHEY_SIMPLEX
    bottomLeftCornerOfText = (50,500)
    fontScale              = 0.7
    fontColor              = (255,255,0)
    lineType               = 2

    text = ""
    predictions = []
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        if ret == True:
            # Our operations on the frame come here
            res, pred = vc.image_classification(frame)

            if res is not None:
                text = '%s' % res
                predictions.append(pred)

            cv2.putText(frame, "Batch predictions:", 
                (30, 450), 
                font, 
                fontScale,
                fontColor,
                lineType)

            cv2.putText(frame, text, 
                bottomLeftCornerOfText, 
                font, 
                fontScale,
                fontColor,
                lineType)

            # cv2.putText(frame, "'SM': Smooth     'NSM': Non-Smooth", 
            cv2.putText(frame, "'G': Glue     'NG': No-Glue", 
                (200, 550), 
                font, 
                fontScale,
                fontColor,
                lineType)

            # Display the resulting frame
            out.write(frame)

            # cv2.imshow('frame', frame)
            
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break
    print("sum:", np.array(predictions)[:-7].sum()/100.0)
    print("sum2:", np.array(predictions).sum()/100.0)
    # When everything done, release the capture
    cap.release()
    out.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()