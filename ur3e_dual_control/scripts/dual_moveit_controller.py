#!/usr/bin/env python2

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, SRI International
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of SRI International nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Acorn Pooley, Mike Lautman

# To use the Python MoveIt interfaces, we will import the `moveit_commander`_ namespace.
# This namespace provides us with a `MoveGroupCommander`_ class, a `PlanningSceneInterface`_ class,
# and a `RobotCommander`_ class. More on these below. We also import `rospy`_ and some messages that we will use:
##

import argparse
import datetime
import sys
import copy
import rosbag
import rospy
import numpy as np
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from ur_control import transformations as tr
from ur_control import conversions as conv
from ur_control.controllers import GripperController
from robotiq_urcap_control.msg import Robotiq2FGripper_robot_input as inputMsg
from robotiq_urcap_control.msg import Robotiq2FGripper_robot_output as outputMsg
from robotiq_urcap_control.gripper import RobotiqGripper
from ur_ikfast import ur_kinematics

import signal

LEFTARM = 0
RIGHTARM = 1


def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)


def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if a list of values are within a tolerance of their counterparts in another list
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    all_equal = True
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        return all_close(pose_to_list(goal), pose_to_list(actual), tolerance)

    return True


class GripperBase():
    def open(self):
        pass

    def close(self):
        pass


class Gripper():
    def __init__(self, robot_ip, prefix=""):
        print("Connecting to gripper:", robot_ip)
        self.controller = RobotiqGripper(robot_ip=robot_ip)
        # The Gripper status is published on the topic named 'Robotiq2FGripperRobotInput'
        pub = rospy.Publisher(
            prefix + 'Robotiq2FGripperRobotInput', inputMsg, queue_size=1)

        # The Gripper command is received from the topic named 'Robotiq2FGripperRobotOutput'
        rospy.Subscriber(prefix + 'Robotiq2FGripperRobotOutput',
                         outputMsg, self.controller.send_command)
        self.controller.connect()
        self.controller.activate(auto_calibrate=False)

    def open(self):
        self.controller.open(speed=64, force=1)

    def close(self):
        self.controller.close(speed=20, force=100)


class DualMoveItControl(object):
    """DualMoveItControl"""

    def __init__(self, load_gripper=False, cautious_mode=True, sim=False):
        super(DualMoveItControl, self).__init__()

        # First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('move_group_python_interface', anonymous=True)

        # Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        # kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        # Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        # for getting, setting, and updating the robot's internal understanding of the
        # surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        # Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        # to a planning group (group of joints).  In this tutorial the group is the primary
        # arm joints in the ur3e robot, so we set the group's name to "ur3e_arm".
        # If you are using a different robot, change this value to the name of your robot
        # arm planning group.
        # This interface can be used to plan and execute motions:
        leftarm_move_group = moveit_commander.MoveGroupCommander("leftarm")
        rightarm_move_group = moveit_commander.MoveGroupCommander("rightarm")

        if load_gripper:
            if sim:
                # self.leftarm_gripper = GripperController(namespace='left_arm', timeout=1.0)
                # self.rightarm_gripper = GripperController(namespace='right_arm', timeout=1.0)
                self.leftarm_gripper = GripperBase()
                self.rightarm_gripper = GripperBase()
            else:
                left_robot_ip = rospy.get_param(
                    "left_arm/ur_hardware_interface/robot_ip")
                right_robot_ip = rospy.get_param(
                    "right_arm/ur_hardware_interface/robot_ip")
                self.leftarm_gripper = Gripper(left_robot_ip, "leftarm_")
                self.rightarm_gripper = Gripper(right_robot_ip, "rightarm_")

        # Create a `DisplayTrajectory`_ ROS publisher which is used to display
        # trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                       moveit_msgs.msg.DisplayTrajectory,
                                                       queue_size=20)

        self.arm_ik = ur_kinematics.URKinematics('ur3e')

        # Misc variables
        self.robot = robot
        self.scene = scene
        self.leftarm_move_group = leftarm_move_group
        self.rightarm_move_group = rightarm_move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.cautious_mode = cautious_mode
        self.plan_history = {}

    def get_move_group(self, arm=LEFTARM):
        if arm == LEFTARM:
            return self.leftarm_move_group
        elif arm == RIGHTARM:
            return self.rightarm_move_group
        else:
            raise Exception("Unsupported value for arm", arm)

    def get_grasping_group(self, arm=LEFTARM):
        if arm == LEFTARM:
            return 'leftarm_ee'
        elif arm == RIGHTARM:
            return 'rightarm_ee'
        else:
            raise Exception("Unsupported value for arm", arm)

    def go_to_joint_state(self, joint_goal, arm=LEFTARM, save_as=None):

        move_group = self.get_move_group(arm)

        previous_plan = self.plan_history.get(save_as)
        if save_as is not None and previous_plan is not None:
            res = move_group.execute(previous_plan)
            if res:
                return True

        plan = move_group.plan(joint_goal)
        self.display_trajectory(plan)

        if self.cautious_mode:
            print "============ Press `Enter` to execute plan ..."
            raw_input()

        move_group.execute(plan)

        if save_as is not None:
            self.plan_history[save_as] = plan

        # For testing:
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def go_to_pose_goal_ik(self, pose_goal, arm=LEFTARM):
        move_group = self.get_move_group(arm)

        current_pose = move_group.get_current_joint_values()

        goal = self.ik(pose_goal, current_pose, arm)

        return self.go_to_joint_state(goal, arm)

    def go_to_pose_goal(self, pose_goal, arm=LEFTARM, save_as=None):
        move_group = self.get_move_group(arm)

        previous_plan = self.plan_history.get(save_as)
        if save_as is not None and previous_plan is not None:
            res = move_group.execute(previous_plan)
            if res:
                return True

        goal = moveit_commander.conversions.list_to_pose(pose_goal)

        plan = move_group.plan(goal)
        self.display_trajectory(plan)

        if self.cautious_mode:
            print "============ Press `Enter` to execute plan ..."
            raw_input()

        move_group.execute(plan)

        if save_as is not None:
            self.plan_history[save_as] = plan

        # For testing:
        current_pose = move_group.get_current_pose().pose
        return all_close(goal, current_pose, 0.01)

    def plan_cartesian_path(self, waypoints, arm=LEFTARM, scale=1):

        move_group = self.get_move_group(arm)

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this tutorial.
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints,   # waypoints to follow
            0.01,        # eef_step
            0.0)         # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def display_trajectory(self, plan):
        # A `DisplayTrajectory`_ msg has two primary fields, trajectory_start and trajectory.
        # We populate the trajectory_start with our current robot state to copy over
        # any AttachedCollisionObjects and add our plan to the trajectory.
        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = self.robot.get_current_state()
        display_trajectory.trajectory.append(plan)
        # Publish
        self.display_trajectory_publisher.publish(display_trajectory)

    def wait_for_state_update(self, obj_name, obj_is_known=False, obj_is_attached=False, timeout=1):

        # Ensuring Collision Updates Are Receieved
        # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        # If the Python node dies before publishing a collision object update message, the message
        # could get lost and the obj will not appear. To ensure that the updates are
        # made, we wait until we see the changes reflected in the
        # ``get_attached_objects()`` and ``get_known_object_names()`` lists.
        # For the purpose of this tutorial, we call this function after adding,
        # removing, attaching or detaching an object in the planning scene. We then wait
        # until the updates have been made or ``timeout`` seconds have passed
        start = rospy.get_time()
        seconds = rospy.get_time()
        while (seconds - start < timeout) and not rospy.is_shutdown():
            # Test if the obj is in attached objects
            attached_objects = self.scene.get_attached_objects([obj_name])
            is_attached = len(attached_objects.keys()) > 0

            # Test if the obj is in the scene.
            # Note that attaching the obj will remove it from known_objects
            is_known = obj_name in self.scene.get_known_object_names()

            # Test if we are in the expected state
            if (obj_is_attached == is_attached) and (obj_is_known == is_known):
                return True

            # Sleep so that we give other threads time on the processor
            rospy.sleep(0.1)
            seconds = rospy.get_time()

        # If we exited the while loop without returning then we timed out
        return False-2.263, 0.3991, -1.9334, -0.1115, 1.6101, 0.6898

    def add_object(self, name, pose, timeout=1, type="box", size=None):
        obj_pose = geometry_msgs.msg.PoseStamped()
        obj_pose.header.frame_id = "workspace_body"
        obj_pose.pose.position = conv.to_point(pose[:3])
        obj_pose.pose.orientation = conv.to_quaternion(
            tr.quaternion_from_euler(*pose[3:]))

        if type == "box":
            self.scene.add_box(name, obj_pose, size=size)
        elif type == "cylinder":
            self.scene.add_cylinder(
                name, obj_pose, height=size[0], radius=size[1])
        return self.wait_for_state_update(name, obj_is_known=True, timeout=timeout)

    def attach_object(self, obj_name, arm=LEFTARM, timeout=1):

        move_group = self.get_move_group(arm)
        grasping_group = self.get_grasping_group(arm)
        eef_link = move_group.get_end_effector_link()

        # Attaching Objects to the Robot
        # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        # Next, we will attach the box to the ur3e wrist. Manipulating objects requires the
        # robot be able to touch them without the planning scene reporting the contact as a
        # collision. By adding link names to the ``touch_links`` array, we are telling the
        # planning scene to ignore collisions between those links and the box. For the ur3e
        # robot, we set ``grasping_group = 'hand'``. If you are using a different robot,
        # you should change this value to the name of your end effector group name.
        touch_links = self.robot.get_link_names(group=grasping_group)
        self.scene.attach_box(eef_link, obj_name, touch_links=touch_links)

        # We wait for the planning scene to update.
        return self.wait_for_state_update(obj_name, obj_is_attached=True, obj_is_known=False, timeout=timeout)

    def detach_object(self, obj_name=None, arm=LEFTARM, timeout=1):

        move_group = self.get_move_group(arm)
        eef_link = move_group.get_end_effector_link()

        # Detaching Objects from the Robot
        # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can also detach and remove the object from the planning scene:
        self.scene.remove_attached_object(eef_link, name=obj_name)

        # We wait for the planning scene to update.
        return self.wait_for_state_update(obj_name, obj_is_known=True, obj_is_attached=False, timeout=timeout)

    def remove_object(self, obj_name=None, timeout=1):

        # Removing Objects from the Planning Scene
        # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can remove the box from the world.
        self.scene.remove_world_object(obj_name)

        # **Note:** The object must be detached before we can remove it from the world

        # We wait for the planning scene to update.
        return self.wait_for_state_update(obj_name, obj_is_attached=False, obj_is_known=False, timeout=timeout)

    def pick_place_tool(self,  pre_grasp_posture, grasp_pose, tool_name, pick=True, arm=LEFTARM):
        '''
            pick bool: If true then try to grasp, If false then try to place
        '''
        # 1. pre grasp pose plan
        # 2. go to grasp pose
        print ">>> grasp pose"
        self.go_to_pose_goal(grasp_pose, arm=arm)

        # 3. pick/attach object or release/detach object
        if pick:
            # self.leftarm_gripper.command(0.8, percentage=True)
            print "============ Press `Enter` to execute plan ..."
            raw_input()
            self.leftarm_gripper.close()
            # self.attach_object(tool_name, arm=arm)
        else:
            self.leftarm_gripper.open()
            # self.leftarm_gripper.open()
            # self.detach_object(tool_name, arm=arm)

        # 4. go back to pre grasp pose
        print ">>> pre/post grasp pose"
        self.go_to_joint_state(pre_grasp_posture, arm=arm)

    def ik(self, global_pose, q_guess, arm=LEFTARM):

        if arm == LEFTARM:
            offset = [0.365, 0.345, 1.330, 0.500, 0.500, 0.500, 0.500]
        else:
            offset = [0.365, -0.345, 1.330, 0.500, 0.500, 0.500, 0.500]
        intermediate_pose = substrac_pose(global_pose, offset)

        extra_ee = [0, 0, -0.1871, 0, 0, 0, 1]
        target_pose = np.array(
            conv.transform_end_effector(intermediate_pose, extra_ee))

        return self.arm_ik.inverse(target_pose, q_guess=q_guess)


def substrac_pose(pose1, pose2):
    rot1 = tr.vector_to_pyquaternion(pose1[3:])
    rot2 = tr.vector_to_pyquaternion(pose2[3:]).inverse
    rot_res = tr.vector_from_pyquaternion(rot2 * rot1)

    position1 = pose1[:3]
    position2 = pose2[:3]
    position_res = np.subtract(position1, position2)
    position_res = rot2.rotate(position_res)

    return np.concatenate([position_res, rot_res])


def main():

    arg_fmt = argparse.RawDescriptionHelpFormatter
    parser = argparse.ArgumentParser(
        formatter_class=arg_fmt, description=main.__doc__)
    parser.add_argument(
        '-i', '--initial-position', action='store_true', help='Move both arms to initial position')
    parser.add_argument(
        '-p', '--grasp-pick',       action='store_true', help='Execute grasp pick holder trajectories')
    parser.add_argument(
        '-s', '--grasp-spreader',   action='store_true', help='Execute grasp spreader holder trajectories')
    parser.add_argument(
        '-r', '--place-pick',       action='store_true', help='Execute place pick holder trajectories')
    parser.add_argument(
        '-a', '--place-spreader',   action='store_true', help='Execute place spreader holder trajectories')
    parser.add_argument(
        '--demo',                   action='store_true', help='Execute all trajectories in sequence')
    parser.add_argument(
        '--sim',                    action='store_true', help='Simulation env')
    parser.add_argument(
        '--cautious',               action='store_true', help='Request user input before executing trajectory')
    parser.add_argument(
        '--remove-objects',         action='store_true', help='Remove rviz objects')

    args = parser.parse_args(rospy.myargv()[1:])

    # Initialize controllers
    dual_controller = DualMoveItControl(
        load_gripper=True, cautious_mode=args.cautious, sim=args.sim)

    # Reset/load models into MoveIt planner
    pick_name = "pick_holder"
    spreader_name = "spreader_holder"
    plan_folder = '/root/ros_ws/src/ros-universal-robots/ur3e_dual_moveit_config/data'

    if args.initial_position:
        dual_controller.leftarm_gripper.open()
        dual_controller.remove_object(pick_name)
        dual_controller.detach_object(pick_name)
        dual_controller.remove_object(spreader_name)
        dual_controller.detach_object(spreader_name)
        dual_controller.remove_object("pipe")

        dual_controller.add_object(
            pick_name,     [1.055, 0.138, 1.032, 0, 0, 0], size=(0.065, 0.035, 0.035))
        dual_controller.add_object(
            spreader_name, [1.055, 0.235, 1.032, 0, 0, 0], size=(0.065, 0.035, 0.035))

    task_initial_pose = [1.8919, -1.7447, -1.0475, -0.347, -0.3216, -0.0027]
    task = [0.915, 0.018, 1.059, 0.5, -0.5, 0.5, 0.5]
    rightarm_init_pose = [0.78, -0.075, 1.042, -0.500, 0.500, -0.500, 0.500]
    right_posture = [-1.1559, -0.7517, 1.7833, -2.603, 1.5705, -1.9794]

    if args.initial_position or args.demo:
        print ">>> Go to Initial Positions"
        dual_controller.go_to_joint_state(task_initial_pose)
        # dual_controller.go_to_pose_goal(task, arm=LEFTARM)
        dual_controller.go_to_joint_state(right_posture, arm=RIGHTARM)
        # dual_controller.go_to_pose_goal(rightarm_init_pose, arm=RIGHTARM)
        # dual_controller.add_object(name="pipe", pose=[0.73, -0.10, 1.06, 0, 1.5707, 0], type="cylinder", size=[0.3, 0.01])

    ### Pick Holder Seq ###
    pre_grasp_pick_holder_posture = [
        1.339, -1.7301, -1.6825, -1.2994, -1.5711, 0.2309]
    grasp_pick_holder_pose = [1.047, 0.138, 1.0312, 0.500, 0.500, 0.500, 0.500]

    if args.grasp_pick or args.demo:
        print ">>> Pick Pick-Holder"
        # 1. go from initial task posture to support (pre_grasp_posture)
        # dual_controller.replay_bag(task_to_support_plan)
        dual_controller.go_to_joint_state(pre_grasp_pick_holder_posture)
        dual_controller.pick_place_tool(pre_grasp_posture=pre_grasp_pick_holder_posture,
                                        grasp_pose=grasp_pick_holder_pose,
                                        pick=True, tool_name=pick_name)
        dual_controller.go_to_joint_state(task_initial_pose)

    if args.place_pick or args.demo:
        print ">>> Place Pick-Holder"
        # 1. go from initial task posture to support (pre_grasp_posture)
        dual_controller.go_to_joint_state(task_initial_pose)
        dual_controller.go_to_joint_state(pre_grasp_pick_holder_posture)
        # dual_controller.replay_bag(task_to_support_plan)
        dual_controller.pick_place_tool(pre_grasp_posture=pre_grasp_pick_holder_posture,
                                        grasp_pose=grasp_pick_holder_pose,
                                        pick=False, tool_name=pick_name)
        ### Pick Seq END ###

    ### Spreader Holder Seq ###
    pre_grasp_spreader_holder_posture = [
        1.6446, -1.538, -1.8893, -1.2841, -1.5706, -0.0739]
    grasp_spreader_holder_pose = [
        1.047, 0.235, 1.0312, 0.500, 0.500, 0.500, 0.500]

    if args.grasp_spreader or args.demo:
        print ">>> From Pick to Spreader Holder"
        # dual_controller.replay_bag(pick_to_spreader)
        dual_controller.go_to_joint_state(pre_grasp_spreader_holder_posture)
        dual_controller.pick_place_tool(pre_grasp_posture=pre_grasp_spreader_holder_posture,
                                        grasp_pose=grasp_spreader_holder_pose,
                                        pick=True, tool_name=spreader_name)
        dual_controller.go_to_joint_state(task_initial_pose)

    if args.place_spreader or args.demo:
        print ">>> Place Spreader-Holder"
        dual_controller.go_to_joint_state(task_initial_pose)
        dual_controller.go_to_joint_state(pre_grasp_spreader_holder_posture)
        # dual_controller.replay_bag(task_to_support_plan)
        dual_controller.pick_place_tool(pre_grasp_posture=pre_grasp_spreader_holder_posture,
                                        grasp_pose=grasp_spreader_holder_pose,
                                        pick=False, tool_name=spreader_name)

    if args.remove_objects:
        dual_controller.remove_object(pick_name)
        dual_controller.remove_object(spreader_name)
        dual_controller.remove_object("pipe")


if __name__ == '__main__':
    main()
