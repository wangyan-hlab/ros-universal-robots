#!/usr/bin/env python

# The MIT License (MIT)
#
# Copyright (c) 2018-2021 Cristian Beltran
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Author: Cristian Beltran

"""
UR Joint Position Example: 3Dconnexion mouse

requires ros-$ROS-VERSION-spacenav-node
and to launch roslaunch spacenav_node classic.launch
#TODO launch automatically
"""
import argparse

import rospy

from ur_control.arm import Arm
from ur_control.constants import IKFAST, TRAC_IK
from ur_control.mouse_6d import Mouse6D
from ur_control import transformations

from ur_ikfast import ur_kinematics

import numpy as np

np.set_printoptions(suppress=True)
ur3e_arm = ur_kinematics.URKinematics('ur3e')
mouse6d = Mouse6D()

axes = 'rxyz'


def e2q(e):
    return transformations.quaternion_from_euler(e[0], e[1], e[2], axes=axes)


def print_robot_state():
    print(("Joint angles:", np.round(arm.joint_angles(), 3)))
    print(("End Effector:", np.round(arm.end_effector(rot_type='euler'), 3)))


def start_control(linear_speed=0.02, angular_speed=3, relative_to_tcp=False):
    """ 
    linear_speed: m/s, Max linear speed.
    angular_speed: deg/s, Max angular speed.
    """
    print("Start moving.")
    rate = rospy.Rate(125)
    delta_x = linear_speed
    delta_q = np.deg2rad(angular_speed)

    right_button_clicked = False
    left_button_clicked = False
    while not rospy.is_shutdown():
        
        if not mouse6d.twist:
            rospy.logerr("Mouse6D data not available!")
            rospy.sleep(1)
            continue

        # Button based events
        if mouse6d.joy_buttons[0] == 1 and not left_button_clicked:
            print_robot_state()
            left_button_clicked = True
            continue
        elif mouse6d.joy_buttons[0] == 0:
            left_button_clicked = False
        
        if mouse6d.joy_buttons[1] == 1 and not right_button_clicked:
            print("Not implemented yet")
            right_button_clicked = True
            continue
        elif mouse6d.joy_buttons[1] == 0:
            right_button_clicked = False

        # Move robot
        cmd_twist = np.array(mouse6d.twist)
        rospy.loginfo_throttle(1, "Mouse6D Twist: %s" % np.round(cmd_twist, 8))
        
        # Scale speed according to Mouse6D twist
        # Limit minimum recognized speed to reduce sensitivity 
        cmd_twist[:3] = [delta_x*np.sign(cmd_twist[i]) if abs(cmd_twist[i]) > 0.15 else 0.0 for i in range(3)]
        cmd_twist[3:] = [delta_q*np.sign(cmd_twist[3+i]) if abs(cmd_twist[3+i]) > 0.15 else 0.0 for i in range(3)]
        
        cmd = transformations.pose_from_angular_velocity(arm.end_effector(), cmd_twist, dt=1/125., ee_rotation=relative_to_tcp)

        arm.set_target_pose_flex(pose=cmd, t=1/125.)
        rate.sleep()


def main():
    """ 3D mouse Control """
    arg_fmt = argparse.RawDescriptionHelpFormatter
    parser = argparse.ArgumentParser(
        formatter_class=arg_fmt, description=main.__doc__)
    parser.add_argument('-r', '--relative', action='store_true', help='move using relative rotation of end-effector')
    parser.add_argument('-a', '--angular',type=float, help='Max angular speed (deg/sec). Default 3.0', default=3.0)
    parser.add_argument('-s', '--linear',type=float, help='Max linear speed (m/s). Default 0.02', default=0.02)
    args = parser.parse_args(rospy.myargv()[1:])

    rospy.init_node("joint_position_keyboard")

    global arm
    arm = Arm(ft_sensor=False, ik_solver=TRAC_IK)

    start_control(linear_speed=args.linear, angular_speed=args.angular, relative_to_tcp=args.relative)
    print("Done.")


if __name__ == '__main__':
    main()
