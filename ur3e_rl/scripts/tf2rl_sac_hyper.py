#!/usr/bin/env python
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #tensorflow logging disabled

from shutil import copyfile

import rospy
import timeit
import argparse
from gym.envs.registration import register

from tf2rl.algos.sac import SAC
from tf2rl.experiments.cb_trainer import Trainer

from ur3e_openai.common import load_environment, log_ros_params, clear_gym_params, load_ros_params
import gym

import sys
import signal

import ur_control.utils as utils
def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


def run_experiment(args, param_file, common_params):
    start_time = timeit.default_timer()
    clear_gym_params('ur3e_gym')

    p = utils.TextColors()
    p.ok("GYM Environment:{} ".format(param_file))
    ros_param_path = load_ros_params(rospackage_name="ur3e_rl",
                    rel_path_from_package_to_file="config",
                    yaml_file_name=param_file)

    # load_ros_params(rospackage_name="ur3e_rl",
    #                 rel_path_from_package_to_file="config",
    #                 yaml_file_name=common_params)
                
    # force_control_params = "simulation/iros20_revised/hybrid.yaml" if "hybrid" in param_file else "simulation/iros20_revised/impedance.yaml"
    # load_ros_params(rospackage_name="ur3e_rl",
    #             rel_path_from_package_to_file="config",
    #             yaml_file_name=force_control_params)


    args.episode_max_steps = rospy.get_param("ur3e_gym/steps_per_episode", 100)

    env = gym.make(rospy.get_param('ur3e_gym/env_id'))
    actor_class = rospy.get_param("ur3e_gym/actor_class")

    policy = SAC(
        state_shape=env.observation_space.shape,
        action_dim=env.action_space.high.size,
        actor_class=actor_class,
        max_action=env.action_space.high[0],
        batch_size=args.batch_size,
        n_warmup=args.n_warmup,
        auto_alpha=args.auto_alpha,
        lr=args.lr
        )
    trainer = Trainer(policy, env, args, test_env=None)
    outdir = trainer._output_dir
    log_ros_params(outdir)
    rospy.set_param('ur3e_gym/output_dir', outdir)
    copyfile(ros_param_path, outdir + "/ros_gym_env_params.yaml")
    trainer()

    print("duration", (timeit.default_timer() - start_time)/60.,"min")

if __name__ == '__main__':

    parser = Trainer.get_argument()
    parser.add_argument('--env-id', type=int, help='environment ID', default=None)
    parser.set_defaults(batch_size=256)
    parser.set_defaults(n_warmup=0) # still don't know what it this for
    parser.set_defaults(max_steps=60000) # 10000 for training 200 for evaluation
    parser.set_defaults(save_model_interval=10000)
    parser.set_defaults(test_interval=2e10) # 1e4 for training 200 for evaluation
    parser.set_defaults(test_episodes=3)
    parser.set_defaults(normalize_obs=False)
    parser.set_defaults(auto_alpha=False)
    parser.set_defaults(use_prioritized_rb=True)
    parser.set_defaults(lr=3e-4)
    parser.set_defaults(model_dir=None)

    args = parser.parse_args(rospy.myargv()[1:])

    rospy.init_node('ur3e_tf2rl',
                    anonymous=True,
                    log_level=rospy.ERROR)

    args = parser.parse_args()
    param_file = None

    experiment_list = [
        ['randinit_512_p14','simulation/mdpi/randinit_p14.yaml', 512],
        ['randinit_1024_p14','simulation/mdpi/randinit_p14.yaml', 1024],
        ['randinit_2048_p14','simulation/mdpi/randinit_p14.yaml', 2048],
        ['randinit_2048_p14','simulation/mdpi/randinit_p14.yaml', 4096],
    ]

    load_environment(
            "UR3eHybridControlEnv-v0",
            max_episode_steps=300, register_only=True)

    repetitions = 1

    for _ in range(repetitions):
        for exp in experiment_list:
            args.dir_suffix = exp[0]
            args.batch_size = exp[2]
            args.n_warmup = exp[2]
            param_file = exp[1]
            common_params = 'simulation/iros20_revised/commons.yaml'
            run_experiment(args, param_file, common_params)
