ur3e_openai.moving_cube package
==============================

Submodules
----------

ur3e_openai.moving_cube.one_disk_walk module
-------------------------------------------

.. automodule:: ur3e_openai.moving_cube.one_disk_walk
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ur3e_openai.moving_cube
    :members:
    :undoc-members:
    :show-inheritance:
