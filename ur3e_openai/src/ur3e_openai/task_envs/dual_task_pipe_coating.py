import timeit
import datetime
import rospy
import rospkg
import numpy as np

from std_msgs.msg import Float32, String

from ur_control.constants import FORCE_TORQUE_EXCEEDED
from ur_control import transformations, spalg
import ur3e_openai.cost_utils as cost

from gym import spaces

from ur3e_openai.robot_envs import dual_ur3e_env
from ur3e_openai.control.parallel_controller_3d import ParallelController3D
from ur3e_openai.control.admittance_controller import AdmittanceController
from ur3e_openai.robot_envs.utils import load_param_vars, save_log, randomize_initial_pose

from ur3e_dual_control.srv import ExecuteMoveItPlan

import subprocess

TASK_INITIAL = 0
APPLYING_GLUE = 1
APPLYING_GLUE_COMPLETED = 2
SPREADING_GLUE = 3
SPREADING_GLUE_COMPLETED = 4
TASK_COMPLETED = 5

from robotiq_urcap_control.msg import Robotiq2FGripper_robot_input as inputMsg
from robotiq_urcap_control.msg import Robotiq2FGripper_robot_output as outputMsg
from robotiq_urcap_control.gripper import RobotiqGripper

class Gripper():
    def __init__(self, robot_ip, prefix=""):
        print("Connecting to gripper:", robot_ip)
        self.controller = RobotiqGripper(robot_ip=robot_ip)
        # The Gripper status is published on the topic named 'Robotiq2FGripperRobotInput'
        pub = rospy.Publisher(
            prefix + 'Robotiq2FGripperRobotInput', inputMsg, queue_size=1)

        # The Gripper command is received from the topic named 'Robotiq2FGripperRobotOutput'
        rospy.Subscriber(prefix + 'Robotiq2FGripperRobotOutput',
                         outputMsg, self.controller.send_command)
        self.controller.connect()
        self.controller.activate(auto_calibrate=False)

    def open(self):
        self.controller.open(speed=64, force=1)

    def close(self):
        self.controller.close(speed=20, force=100)

class UR3eDualTaskPipeCoatingEnv(dual_ur3e_env.DualUR3eEnv):
    def __init__(self):

        self.cost_positive = False
        self.get_robot_params()

        dual_ur3e_env.DualUR3eEnv.__init__(self)

        self.controller = ParallelController3D(
            self.left_ur3e_arm, self.agent_control_dt)

        self._previous_joints = None
        self.obs_logfile = None
        self.reward_per_step = []
        self.obs_per_step = []
        self.action_result = None

        self.last_actions = np.zeros(self.n_actions)
        self.pipe_classifier_result = 0.0
        self.success_counter = 0
        obs = self._get_obs()

        self.reward_threshold = 500.0

        self.action_space = spaces.Box(-1., 1.,
                                       shape=(self.n_actions, ),
                                       dtype='float32')

        self.observation_space = spaces.Box(-np.inf,
                                            np.inf,
                                            shape=obs.shape,
                                            dtype='float32')

        self.trials = 1
        self.task_state = TASK_INITIAL
        self.task_completed = False

        self.pipe_classifier_sub = rospy.Subscriber(
            "/pipe_classifier/result", Float32, callback=self._pipe_classifier_cb, queue_size=1)
        
        if not self.simulation:
            self.reset_right_arm_pub = rospy.Publisher("/right_arm/ur_hardware_interface/script_command", String, queue_size=1)

        rospy.wait_for_service('moveit_ur3e_dual')
        self.moveit_planner_service = rospy.ServiceProxy('moveit_ur3e_dual', ExecuteMoveItPlan)

        left_robot_ip = rospy.get_param("left_arm/ur_hardware_interface/robot_ip")
        self.leftarm_gripper = Gripper(left_robot_ip, "leftarm_")

        rospy.loginfo("ACTION SPACES TYPE: %s" % (self.action_space))
        rospy.loginfo("OBSERVATION SPACES TYPE: %s" % (self.observation_space))

    def get_robot_params(self):
        prefix = "ur3e_gym"
        load_param_vars(self, prefix)

        self.ft_topic = rospy.get_param(prefix + "/ft_topic", None)
        self.param_use_gazebo = False

        self.relative_to_ee = rospy.get_param(
            prefix + "/relative_to_ee", False)

        self.true_target_pose = rospy.get_param(prefix + "/target_pos", False)
        self.rand_seed = rospy.get_param(prefix + "/rand_seed", None)
        self.ft_hist = rospy.get_param(prefix + "/ft_hist", False)
        self.rand_init_interval = rospy.get_param(
            prefix + "/rand_init_interval", 5)
        self.rand_init_counter = 0
        self.rand_init_cpose = None
        self.wrench_hist_size = rospy.get_param(
            prefix + "/wrench_hist_size", 12)

        self.n_successes = rospy.get_param(prefix + "/n_successes", 3)

        self.subtask_duration = self.agent_control_dt*self.steps_per_episode/2.0 + 1.0
        rospy.loginfo("subtask duration: %s" % self.subtask_duration)
        self.dual_tool = rospy.get_param(prefix + "/dual_tool", False)

    def _pipe_classifier_cb(self, msg):
        self.pipe_classifier_result = msg.data

    def _get_obs(self):
        """
        Here we define what sensor data of our robots observations
        To know which Variables we have acces to, we need to read the
        MyRobotEnv API DOCS
        :return: observations
        """
        joint_angles = self.left_ur3e_arm.joint_angles()
        ee_points, ee_velocities = self.get_points_and_vels(joint_angles)
        pipe_classifier_result = np.copy(self.pipe_classifier_result)

        if pipe_classifier_result == 1.0:
            self.success_counter +=1
        if self.success_counter == self.n_successes and self.task_state == APPLYING_GLUE:
            self.success_counter = 0
            self.task_state == APPLYING_GLUE_COMPLETED
        elif self.success_counter == self.n_successes and self.task_state == SPREADING_GLUE:
            self.success_counter = 0
            self.task_state == SPREADING_GLUE_COMPLETED

        obs = None

        desired_force_wrt_goal = np.zeros(6)
        if self.ft_hist:
            force_torque = (self.left_ur3e_arm.get_ee_wrench_hist(
                self.wrench_hist_size) - desired_force_wrt_goal) / self.controller.max_force_torque
            force = force_torque[:,:3]

            obs = np.concatenate([
                ee_points.ravel(),  # [3]
                ee_velocities.ravel(),  # [3]
                self.last_actions.ravel(),  # [12]
                [pipe_classifier_result],
                force.ravel(),  # [3]*12
            ])
            self.obs_per_step = obs[:-(self.wrench_hist_size*3 + 3)]
        else:
            force_torque = (self.left_ur3e_arm.get_ee_wrench(
            ) - desired_force_wrt_goal) / self.controller.max_force_torque
            force = force_torque[:3]

            obs = np.concatenate([
                ee_points.ravel(),  # [3]
                ee_velocities.ravel(),  # [3]
                self.last_actions.ravel(),  # [12]
                [pipe_classifier_result],
                force.ravel(),  # [3]
            ])
            self.obs_per_step = obs

        return obs.copy()

    def get_points_and_vels(self, joint_angles):
        """
        Helper function that gets the cartesian positions
        and velocities from ROS."""

        if self._previous_joints is None:
            self._previous_joints = self.left_ur3e_arm.joint_angles()

        # Current position
        ee_pos_now = self.left_ur3e_arm.end_effector(joint_angles=joint_angles)

        # Last position
        ee_pos_last = self.left_ur3e_arm.end_effector(
            joint_angles=self._previous_joints)
        self._previous_joints = joint_angles  # update

        # Use the past position to get the present velocity.
        velocity = (ee_pos_now[:3] - ee_pos_last[:3]) / self.agent_control_dt

        # Shift the present poistion by the End Effector target.
        # Since we subtract the target point from the current position, the optimal
        # value for this will be 0.
        error = self.target_pos - ee_pos_now[:3]

        # scale error error, for more precise motion
        # (numerical error with small numbers?)
        error *= [1000, 1000, 1000]

        return error, velocity

    def _right_arm_reset(self):
        if self.simulation:
            return
        # reset rightarm wrist position
        # self.rightarm_reset_revol_counter_service()
        rate = rospy.Rate(10) # 1hz
        initime = rospy.get_time()
        timeout = 1
        while not rospy.is_shutdown() and (rospy.get_time() - initime) < timeout:
            self.reset_right_arm_pub.publish("reset_revolution_counter()")
            rate.sleep()
        self._reset_driver_connection()

    def _set_init_pose(self):
        rospy.loginfo("Setting Initial Pose")

        self._log()
        self._right_arm_reset()
        self._sanity_checks()
        self.moveit_planner_service(initialPose=True)
        self.left_ur3e_arm.set_wrench_offset(True)
        self.controller.reset()
        self.success_counter = 0
        self.task_completed = False

    def _log(self):
        if self.obs_logfile is None:
            try:
                self.obs_logfile = rospy.get_param("ur3e_gym/output_dir") + "/state_" + \
                    datetime.datetime.now().strftime('%Y%m%dT%H%M%S') + '.npy'
                rospy.loginfo("obs_logfile %s" % self.obs_logfile)
            except Exception:
                return
        save_log(self.obs_logfile, self.obs_per_step, self.reward_per_step, self.cost_ws)
        self.reward_per_step = []
        self.obs_per_step = []

    def _compute_reward(self, observations, done):
        """
        Return the reward based on the observations given
        """
        pipe_prediction = observations[9]
        
        reward = pipe_prediction + self.step_cost
        if done:
            reward += self.success_reward

        return reward

    def _sanity_checks(self):
        if self.task_state == APPLYING_GLUE:
            self.moveit_planner_service(placePick=True)
        elif self.task_state == SPREADING_GLUE:
            self.moveit_planner_service(placeSpreader=True)
        self.task_state = TASK_INITIAL

    def _is_done(self, observations):
        if self.action_result == FORCE_TORQUE_EXCEEDED:
            self._sanity_checks()
        return self.task_completed == True or self.action_result == FORCE_TORQUE_EXCEEDED

    def _init_env_variables(self):
        self.step_count = 0
        self.action_result = None
        self.last_actions = np.zeros(self.n_actions)

    def _rotate_right_arm(self):
        right_cmd = self.right_ur3e_arm.joint_angles()
        right_cmd[-1] -= np.deg2rad(360.0*self.right_arm_rps)
        print("RPMs right arm:", (self.subtask_duration/self.right_arm_rps))
        self.right_ur3e_arm.set_joint_positions(position=right_cmd, t=self.subtask_duration, wait=False)

    def _prepare_next_step(self, task:str, initial_q, target_pose):
        pick_bool = (task == "glue")
        spread_bool = (task == "smooth")
        
        assert pick_bool or spread_bool, "Invalid task"
        
        if spread_bool:
            rospy.loginfo("Changing tasks t: %s" % (timeit.default_timer() - self.timer))
            self._right_arm_reset()

        rospy.loginfo("Setting task: %s" % task)

        if self.dual_tool:
            self.moveit_planner_service(graspPick=pick_bool, graspSpreader=spread_bool, placePick=spread_bool)
        else:
            self.left_ur3e_arm.set_joint_positions(self.task_initial_pose, t=1.0, wait=True)
        
        rospy.loginfo(">>> Press `Enter` to execute plan <<<")
        input()

        self.left_ur3e_arm.set_joint_positions(initial_q, t=2.0, wait=True)
        self.target_pos = np.copy(target_pose)
        
        # rospy.loginfo(">>> Press `Enter` to execute plan <<<")
        # input()

        self.left_ur3e_arm.set_wrench_offset(True)

        rospy.set_param("/pipe_classifier/stage", task)

        self._previous_joints = self.left_ur3e_arm.joint_angles()
        
        subprocess.Popen(["rosbag", "record", "--duration="+str(int(self.subtask_duration+5)), "/camera/color/image_raw/compressed", "-o"+task])
        self.timer = timeit.default_timer()
        self._rotate_right_arm()

    def _task_end_sequence(self):
        self.moveit_planner_service(placeSpreader=True)

    def _update_task(self):
        if self.task_state == TASK_INITIAL:
            self._prepare_next_step(task="glue", initial_q=self.init_q_pick, target_pose=self.target_pos_pick)
            self.task_state = APPLYING_GLUE
            return True
        elif self.task_state == APPLYING_GLUE_COMPLETED:
            self._prepare_next_step(task="smooth", initial_q=self.init_q_spreader, target_pose=self.target_pos_spreader)
            self.task_state = SPREADING_GLUE
            return True
        elif self.task_state == SPREADING_GLUE_COMPLETED:
            if self.dual_tool:
                self._task_end_sequence()
            else:
                self.left_ur3e_arm.set_joint_positions(self.task_initial_pose, t=1.0, wait=True)

            self.task_state = TASK_INITIAL
            self.task_completed = True
            return False
        return True

    def sine_trajectory(self, axis, target, amplitude=0.01, period=1.0):
        new_target = np.copy(target)
        sine_t = np.sin(np.deg2rad(self.step_count * period)) * amplitude # each step is considered 1 deg
        new_target[axis] += sine_t
        return new_target

    def _set_action(self, action):
        # Ugly workaround to "env.reset()" 
        # Avoid any unnecessary preparation for a false start
        if self.step_count == 0:
            return

        # Let's start
        if self._update_task():
            if self.task_state == APPLYING_GLUE:
                new_target = self.sine_trajectory(2, self.target_pos, period=10.0)
                self.action_result = self.controller.act(action, new_target)
            else:
                self.action_result = self.controller.act(action, self.target_pos)
            
            if self.step_count == int(self.steps_per_episode / 2):
                rospy.loginfo("Applying glue TIMEOUT")
                self.task_state = APPLYING_GLUE_COMPLETED
            elif self.step_count == self.steps_per_episode-2:
                rospy.loginfo("Episode TIMEOUT t: %s" % (timeit.default_timer() - self.timer))
                self.task_state = SPREADING_GLUE_COMPLETED
