#!/usr/bin/env python
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #tensorflow logging disabled

from shutil import copyfile

import rospy
import timeit
import argparse
from gym.envs.registration import register

from tf2rl.algos.sac import SAC
from tf2rl.experiments.cb_trainer import Trainer

from ur3e_openai.common import load_environment, log_ros_params, clear_gym_params, load_ros_params
import gym

import sys
import signal

import ur_control.utils as utils
def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


def run_experiment(args, param_file, common_params):
    start_time = timeit.default_timer()

    clear_gym_params('ur3e_gym')
    clear_gym_params('ur3e_force_control')

    p = utils.TextColors()
    p.ok("GYM Environment:{} ".format(param_file))
    ros_param_path = load_ros_params(rospackage_name="ur3e_rl",
                    rel_path_from_package_to_file="config",
                    yaml_file_name=param_file)

    # load_ros_params(rospackage_name="ur3e_rl",
    #                 rel_path_from_package_to_file="config",
    #                 yaml_file_name=common_params)
                
    # force_control_params = "simulation/iros20_revised/hybrid.yaml" if "hybrid" in param_file else "simulation/iros20_revised/impedance.yaml"
    # load_ros_params(rospackage_name="ur3e_rl",
    #             rel_path_from_package_to_file="config",
    #             yaml_file_name=force_control_params)


    args.episode_max_steps = rospy.get_param("ur3e_gym/steps_per_episode", 100)

    env = load_environment(
            rospy.get_param('ur3e_gym/env_id'),
            max_episode_steps=args.episode_max_steps)
    actor_class = rospy.get_param("ur3e_gym/actor_class")

    policy = SAC(
        state_shape=env.observation_space.shape,
        action_dim=env.action_space.high.size,
        actor_class=actor_class,
        max_action=env.action_space.high[0],
        batch_size=args.batch_size,
        n_warmup=args.n_warmup,
        auto_alpha=args.auto_alpha,
        lr=args.lr
        )

    args.use_prioritized_rb = rospy.get_param("ur3e_gym/use_prioritized_rb", True)
    args.use_prioritized_rb = rospy.get_param("ur3e_gym/auto_alpha", False)

    trainer = Trainer(policy, env, args, test_env=None)
    outdir = trainer._output_dir
    log_ros_params(outdir)
    rospy.set_param('ur3e_gym/output_dir', outdir)
    copyfile(ros_param_path, outdir + "/ros_gym_env_params.yaml")
    trainer()

    print("duration", (timeit.default_timer() - start_time)/60.,"min")

if __name__ == '__main__':

    parser = Trainer.get_argument()
    parser.add_argument('-e', '--env_id', type=int, help='environment ID', default=None)
    parser.set_defaults(batch_size=2048)
    parser.set_defaults(n_warmup=0) # still don't know what it this for
    parser.set_defaults(max_steps=500000) # 10000 for training 200 for evaluation
    parser.set_defaults(save_model_interval=10000)
    parser.set_defaults(test_interval=3000) # 1e4 for training 200 for evaluation
    parser.set_defaults(test_episodes=2)
    parser.set_defaults(normalize_obs=False)
    parser.set_defaults(auto_alpha=False)
    parser.set_defaults(use_prioritized_rb=True)
    parser.set_defaults(lr=3e-4)
    parser.set_defaults(model_dir=None)

    args = parser.parse_args(rospy.myargv()[1:])

    rospy.init_node('ur3e_tf2rl',
                    anonymous=True,
                    log_level=rospy.ERROR)

    args = parser.parse_args()
    param_file = None

    experiment_list = [
        ['overfit','simulation/mdpi/tune/overfit.yaml', 'results/big_policy_actions'],
        # ['auto_alpha','simulation/mdpi/tune/auto_alpha.yaml', None],
        # ['important_sampling','simulation/mdpi/tune/importante_sampling.yaml', None],
        # ['default_state_std','simulation/mdpi/tune/defalut_state_std.yaml', None],
        # ['tcn_policy','simulation/mdpi/randinit_p14.yaml', None],
        # ['nn_policy','simulation/mdpi/tune/nn_policy.yaml', None],
        # ['impedance_13pd', 'simulation/iser20/impedance13pd.yaml', 'results/20200708T175522.461702_SAC_impedance_13pd']
        # ['5deg_randgoal_p24','simulation/mdpi/randgoal_p14.yaml', 'results/20200805T184046.891244_SAC_5deg_randgoal_p24'],
        # ['randstiff_p24','simulation/mdpi/randstiff_p14.yaml', None],
        # ['slow_randinit_p24','simulation/mdpi/randinit_p14.yaml', None],
        # ['rand_all_p24','simulation/mdpi/rand_all_p14.yaml', None],
        # ['randerror_p24','simulation/mdpi/randerror_p14.yaml', None],
        # ['experimental','simulation/mdpi/curriculum_p14.yaml', 'results/20200812T120835.001452_SAC_best_10N_100k_p24']
        # ['conical_p25','simulation/conical_helix_p25.yaml', 'results/20200727T211331.205949_SAC_conical_p25'],
        # ['one_pose_randstiff_p14','simulation/mdpi/one_pose_randstiff_p14.yaml', None],
        # ['one_pose_randerror_p14','simulation/mdpi/one_pose_randerror_p14.yaml', None],
        # ['randerror_x_step_p14','simulation/mdpi/randerror_p14.yaml', 'results/SAC_randerror_p14'],
        # ['one_pose_randstifferror_p14','simulation/mdpi/one_pose_randstifferror_p14.yaml', None],
    ]

    repetitions = 1

    for _ in range(repetitions):
        for exp in experiment_list:
            args.dir_suffix = exp[0]
            args.model_dir = exp[2]
            param_file = exp[1]
            common_params = 'simulation/iros20_revised/commons.yaml'
            run_experiment(args, param_file, common_params)
