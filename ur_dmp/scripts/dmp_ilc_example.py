#!/usr/bin/env python3

from ur_dmp.dmp import CartesianDMP
from ur_control.compliant_controller import CompliantController
from ur_control.constants import IKFAST, TRAC_IK
import argparse
import rospy
import timeit
import pickle
import os
import numpy as np
np.set_printoptions(suppress=True)
np.set_printoptions(linewidth=np.inf)
PATH, _ = os.path.split(__file__)


def move_init(wait=True, t=1.0):
    arm.set_target_pose(pose=[-0.14, 0.42, 0.376, 1., 0., 0., 0.], wait=wait, t=t)


def dmp(path_file, goal_delta, mode, preprocess=False):
    # Move along a recorded curve path
    # move_joints(q=[-0.14, 0.42, 0.376, 1., 0., 0., 0.], t=0.1)
    PREPROCESS = preprocess  # read the recorded joint configuration file and transfer it into a cpose file
    STRIDE = 5

    if PREPROCESS:
        conf_list = np.load(PATH + "/trajectory.pickle", allow_pickle=True)
        coarse_conf_list = []
        for index, p in enumerate(conf_list):
            if index % STRIDE == 0:
                coarse_conf_list.append(conf_list[index])
        cpose_list = []
        for i, jnt in enumerate(coarse_conf_list):
            arm.set_joint_positions(position=jnt, wait=True, t=0.1)
            cpose_list.append(arm.end_effector())
        # Record corresponding robot eef poses of the recorded path
        pickle.dump(cpose_list, open(PATH+"/"+path_file, 'wb'))

    # Try DMP
    old_path = np.load(PATH+"/"+path_file, allow_pickle=True)
    n_steps = len(old_path)
    dt = 0.01
    execution_time = (n_steps - 1) * dt
    T = np.linspace(0, execution_time, n_steps)
    Y = np.empty((n_steps, 7))

    for i, pt in enumerate(old_path):
        pos, rot_q = pt[:3], pt[3:]
        Y[i] = (np.concatenate((pos, rot_q)))

    dmp = CartesianDMP(execution_time=execution_time, dt=dt, n_weights_per_dim=10)
    dmp.imitate(T, Y)
    new_goal_pos = Y[-1][:3] + np.array(([goal_delta[0], goal_delta[1], goal_delta[2]]))
    new_goal_rotq = Y[-1][3:]
    new_goal = np.concatenate((new_goal_pos, new_goal_rotq))
    dmp.goal_y = new_goal
    if mode == 'openloop':
        _, Y = dmp.open_loop()
        for pt in Y:
            arm.set_target_pose_flex(pt, t=0.1)
    elif mode == 'stepbystep':
        last_y = None
        last_yd = None
        count = 0
        while np.linalg.norm(arm.end_effector()[:3] - new_goal[:3]) > 5e-4:
            count += 1
            print('-----\nstep = ', count)
            error = np.linalg.norm(arm.end_effector()[:3] - new_goal[:3])
            print('error = ', error)
            if last_y is None:
                last_y = arm.end_effector()
            else:
                last_y = dmp.current_y
            if last_yd is None:
                last_yd = np.zeros(6)
            else:
                last_yd = dmp.current_yd
            dmp.step(last_y, last_yd)
            print("current_y = ", dmp.current_y)
            print("goal_y = ", new_goal)
            arm.set_target_pose_flex(dmp.current_y, t=0.1)
            if count > 1000:
                rospy.logerr("#Timestep larger than the limit!")
                break
        print('final_error = ', np.linalg.norm(arm.end_effector()[:3] - new_goal[:3]))

def main():
    """ Main function to be run. """
    parser = argparse.ArgumentParser(description='Test force control')
    parser.add_argument('--namespace', type=str,
                        help='Namespace of arm', default=None)
    parser.add_argument('-i', '--init', action='store_true',
                        help='move to initial configuration')
    parser.add_argument('-d', '--dmp', action='store_true',
                        help='modify a trajectory with DMPs and execute it')
    parser.add_argument('--file', type=str,
                        help='pathfile name', default=None)
    parser.add_argument('--goal_delta', nargs=3, type=float,
                        help='offset of the new goal', default=[0., 0., 0.])
    parser.add_argument('--mode', type=str,
                        help='dmp mode', default=None)
    args = parser.parse_args()

    rospy.init_node('ur5e_dmp_control')

    ns = ''
    joints_prefix = None
    robot_urdf = "ur5e"
    tcp_link = None

    if args.namespace:
        ns = args.namespace
        joints_prefix = args.namespace + '_'

    global arm
    arm = CompliantController(ft_sensor=True,
                              namespace=ns,
                              joint_names_prefix=joints_prefix,
                              robot_urdf=robot_urdf,
                              ee_link=tcp_link)
    if args.init:
        move_init()
    if args.dmp:
        dmp(args.file, args.goal_delta, args.mode)


if __name__ == '__main__':
    main()
