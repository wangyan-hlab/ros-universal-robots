import numpy as np
from ur3e_openai.robot_envs.utils import load_param_vars
from ur_control.compliant_controller import CompliantController

class Controller:
    def __init__(self, arm: CompliantController, agent_control_dt):
        self.ur3e_arm = arm
        self.agent_control_dt = agent_control_dt

        self.param_prefix = "ur3e_force_control"
        load_param_vars(self, self.param_prefix)
        self.target_force_torque = self.desired_force_torque

    def reset(self):
        """Reset controller"""
        raise NotImplementedError()

    def act(self, action, target):
        """Execute action on controller"""
        raise NotImplementedError()