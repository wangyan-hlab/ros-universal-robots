Sawyer Environment
==================

Sawyer is the new robot from **Rethink Robotics**. Its a simplified version of the **Baxter Robot**.
Here is the official page for more details: SawyerPage_

.. _SawyerPage: https://www.rethinkrobotics.com/sawyer/


.. figure:: img/sawyer_robot.png
   :scale: 100 %
   :alt: Sawyer Robot


This simulation uses all the SDK systems that the real robot does , so anything you develop for
the simulation should work seamlessly with the real robot.

.. figure:: img/sawyer_sim.png
   :scale: 100 %
   :alt: Simulated Sawyer



Robot Environment
*****************

ur3e_openai.robot_envs.sawyer_env module
---------------------------------------

.. automodule:: ur3e_openai.robot_envs.sawyer_env
    :members:
    :undoc-members:
    :show-inheritance:


Task Environments
*****************

.. toctree::
   :maxdepth: 4

   ur3e_openai.task_envs.sawyer
