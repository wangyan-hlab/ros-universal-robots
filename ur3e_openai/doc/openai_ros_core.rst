OpenAI ROS Core
===============

This is the heart of OpenAI ROS. Here you can finc the classes that tie ROS, Gazebo and OpenAi together.


ur3e_openai.robot_env module
----------------------------------

.. automodule:: ur3e_openai.robot_env
    :members:
    :undoc-members:
    :show-inheritance:

ur3e_openai.gazebo_connection module
-----------------------------------

.. automodule:: ur3e_openai.gazebo_connection
    :members:
    :undoc-members:
    :show-inheritance:

ur3e_openai.controllers_connection module
----------------------------------------

.. automodule:: ur3e_openai.controllers_connection
    :members:
    :undoc-members:
    :show-inheritance: