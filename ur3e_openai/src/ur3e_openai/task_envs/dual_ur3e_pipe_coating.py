import datetime
import rospy
import rospkg
import numpy as np

from ur_control.constants import FORCE_TORQUE_EXCEEDED
from ur_control import transformations, spalg
import ur3e_openai.cost_utils as cost

from gym import spaces

from ur3e_openai.robot_envs import dual_ur3e_env
from ur3e_openai.control.parallel_controller_3d import ParallelController3D
from ur3e_openai.control.admittance_controller import AdmittanceController
from ur3e_openai.robot_envs.utils import load_param_vars, save_log, randomize_initial_pose

import subprocess

class UR3ePipeCoatingEnv(dual_ur3e_env.DualUR3eEnv):
    def __init__(self):

        self.cost_positive = False
        self.get_robot_params()

        dual_ur3e_env.DualUR3eEnv.__init__(self)

        self.controller = ParallelController3D(self.left_ur3e_arm, self.agent_control_dt)

        self._previous_joints = None
        self.obs_logfile = None
        self.reward_per_step = []
        self.obs_per_step = []
        self.max_dist = None
        self.action_result = None

        self.last_actions = np.zeros(self.n_actions)
        obs = self._get_obs()

        self.reward_threshold = 500.0

        self.action_space = spaces.Box(-1., 1.,
                                       shape=(self.n_actions, ),
                                       dtype='float32')

        self.observation_space = spaces.Box(-np.inf,
                                            np.inf,
                                            shape=obs.shape,
                                            dtype='float32')

        self.trials = 1

        print("ACTION SPACES TYPE", (self.action_space))
        print("OBSERVATION SPACES TYPE", (self.observation_space))

    def get_robot_params(self):
        prefix = "ur3e_gym"
        load_param_vars(self, prefix)

        self.ft_topic = rospy.get_param(prefix + "/ft_topic", None)

        self.param_use_gazebo = False

        self.relative_to_ee = rospy.get_param(prefix + "/relative_to_ee", False)

        self.true_target_pose = rospy.get_param(prefix + "/target_pos", False)
        self.rand_seed = rospy.get_param(prefix + "/rand_seed", None)
        self.ft_hist = rospy.get_param(prefix + "/ft_hist", False)
        self.rand_init_interval = rospy.get_param(prefix + "/rand_init_interval", 5)
        self.rand_init_counter = 0
        self.rand_init_cpose = None
        self.wrench_hist_size = rospy.get_param(prefix + "/wrench_hist_size", 12)
        
    def _get_obs(self):
        """
        Here we define what sensor data of our robots observations
        To know which Variables we have acces to, we need to read the
        MyRobotEnv API DOCS
        :return: observations
        """
        joint_angles = self.left_ur3e_arm.joint_angles()
        ee_points, ee_velocities = self.get_points_and_vels(joint_angles)

        obs = None

        # desired_force_wrt_goal = -1.0 * spalg.convert_wrench(self.controller.target_force_torque, self.target_pos)[:3]
        desired_force_wrt_goal = np.zeros(6)
        if self.ft_hist:
            force_torque = (self.left_ur3e_arm.get_ee_wrench_hist(self.wrench_hist_size) - desired_force_wrt_goal) / self.controller.max_force_torque
            force = force_torque[:3]

            obs = np.concatenate([
                ee_points.ravel(),  # [6]
                ee_velocities.ravel(),  # [6]
                desired_force_wrt_goal[:3].ravel(),
                self.last_actions.ravel(),  # [14]
                force.ravel(),  # [3]*24
            ])
        else:
            force_torque = (self.left_ur3e_arm.get_ee_wrench() - desired_force_wrt_goal) / self.controller.max_force_torque
            force = force_torque[:3]

            obs = np.concatenate([
                ee_points.ravel(),  # [6]
                ee_velocities.ravel(),  # [6]
                force.ravel(),  # [3]
                self.last_actions.ravel(),  # [14]
            ])

        return obs.copy()

    def get_points_and_vels(self, joint_angles):
        """
        Helper function that gets the cartesian positions
        and velocities from ROS."""

        if self._previous_joints is None:
            self._previous_joints = self.left_ur3e_arm.joint_angles()

        # Current position
        ee_pos_now = self.left_ur3e_arm.end_effector(joint_angles=joint_angles)

        # Last position
        ee_pos_last = self.left_ur3e_arm.end_effector(joint_angles=self._previous_joints)
        self._previous_joints = joint_angles  # update

        # Use the past position to get the present velocity.
        velocity = (ee_pos_now[:3] - ee_pos_last[:3]) / self.agent_control_dt

        # Shift the present poistion by the End Effector target.
        # Since we subtract the target point from the current position, the optimal
        # value for this will be 0.
        error = self.target_pos - ee_pos_now[:3]


        # scale error error, for more precise motion
        # (numerical error with small numbers?)
        error *= [1000, 1000, 1000]

        return error, velocity

    def _right_arm_reset(self):
        # reset rightarm wrist position
        right_current_pos = self.right_ur3e_arm.joint_angles()
        right_diff = right_current_pos[-1] - (-3.5562)
        right_t = right_diff * 3.0 / (2*np.pi)
        # print("right_t", right_t)
        right_current_pos[-1] = -3.5562
        if right_t > 0.1:
            self.right_ur3e_arm.set_joint_positions(right_current_pos, t=right_t, wait=True)

    def _dual_moveit_control(self, args):
        """Sets the Robot in its init pose
           using MoveIt (Python 2.7)
        """
        rospath = rospkg.RosPack().get_path('ur3e_dual_control')
        python3_command = rospath + "/scripts/dual_moveit_controller.py --sim " + args  # launch your python2 script using bash
        process = subprocess.Popen(python3_command.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()

    def _set_init_pose(self):
        print("Setting Initial Pose")
        self._log()
        self._right_arm_reset()
        self._dual_moveit_control("-i")
        self.left_ur3e_arm.set_wrench_offset(True)
        self.controller.reset()
        self.max_distance = (self.target_pos - self.left_ur3e_arm.end_effector()[:3]) * 1000.
        self.max_dist = None

    def _randomize_initial_pose(self, override=False):
        if self.rand_init_cpose is None or self.rand_init_counter >= self.rand_init_interval or override:
            self.rand_init_cpose = randomize_initial_pose(self.left_ur3e_arm.end_effector(self.init_q), self.workspace, self.reset_time)
            self.rand_init_counter = 0
        self.rand_init_counter += 1

    def _log(self):
        # Test
        # log_data = np.array([self.controller.force_control_model.update_data,self.controller.force_control_model.error_data])
        # print("Hellooo",log_data.shape)
        # logfile = rospy.get_param("ur3e_gym/output_dir") + "/log_" + \
        #             datetime.datetime.now().strftime('%Y%m%dT%H%M%S') + '.npy'
        # np.save(logfile, log_data)
        if self.obs_logfile is None:
            try:
                self.obs_logfile = rospy.get_param("ur3e_gym/output_dir") + "/state_" + \
                    datetime.datetime.now().strftime('%Y%m%dT%H%M%S') + '.npy'
                print("obs_logfile", self.obs_logfile)
            except Exception:
                return
        # save_log(self.obs_logfile, self.obs_per_step, self.reward_per_step, self.cost_ws)
        self.reward_per_step = []
        self.obs_per_step = []

    def _compute_reward(self, observations, done):
        """
        Return the reward based on the observations given
        """
        state = []
        if self.ft_hist:
            ft_size = self.wrench_hist_size*3
            state = np.concatenate([
                observations[:-ft_size].ravel(),
                observations[-3:].ravel(),
                [self.action_result]
            ])
        else:
            state = np.concatenate([
                observations.ravel(),
                [self.action_result]
            ])
        self.obs_per_step.append([state])

        # TODO define cost function
        return 0

    def _is_done(self, observations):
        # TODO: based on pipe classifier
        return False or self.action_result == FORCE_TORQUE_EXCEEDED

    def _init_env_variables(self):
        self.step_count = 0
        self.action_result = None
        self.last_actions = np.zeros(self.n_actions)

    def _set_action(self, action):
        right_cmd = self.right_ur3e_arm.joint_angles()
        right_cmd[-1] += np.deg2rad(360.0 * self.agent_control_dt / self.right_arm_spr) 
        self.right_ur3e_arm.set_joint_positions_flex(position=right_cmd, t=self.agent_control_dt)
        self.action_result = self.controller.act(action, self.target_pos)
