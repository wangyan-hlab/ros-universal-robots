#!/usr/bin/env python3
# Author: WY

from ur_control import spalg, transformations, traj_utils
from ur_control.arm import Arm
from ur_control.constants import IKFAST, TRAC_IK
import argparse
import rospy
import timeit
import numpy as np
from pyquaternion import Quaternion
np.set_printoptions(suppress=True)
np.set_printoptions(linewidth=np.inf)

def get_jnt():
    rospy.loginfo(f"jnt_angles (rad) = {arm.joint_angles()}")
    rospy.loginfo(f"jnt_angles (deg) = {np.rad2deg(arm.joint_angles())}")

def get_pose():
    rospy.loginfo(f"cpose = {arm.end_effector()}")

def move_joints(wait=True):
    # q = [1.5353, -1.211, -1.4186, -0.546, 1.6476, -0.0237]    # hlab-ur3e initial pose
    # q = [0.8166, -1.935, - 1.9989, 0.1319, 1.8946, 0.1323]    # hlab-ur3e behind the cube
    q = [1.57, -1.57, 1.57, -1.57, -1.57,  0.0]                 # cube-ur5 initial pose
    arm.set_joint_positions(position=q, wait=wait, t=1.0)

def move_x(delta, wait=True):
    cpose = arm.end_effector()
    deltax = np.array([delta, 0., 0., 0., 0., 0.])
    cpose = transformations.pose_euler_to_quaternion(cpose, deltax, ee_rotation=True)
    arm.set_target_pose(pose=cpose, wait=wait, t=1.0)

def move_y(delta, wait=True):
    cpose = arm.end_effector()
    deltay = np.array([0., delta, 0., 0., 0., 0.])
    cpose = transformations.pose_euler_to_quaternion(cpose, deltay, ee_rotation=True)
    arm.set_target_pose(pose=cpose, wait=wait, t=1.0)

def move_z(delta, wait=True):
    cpose = arm.end_effector()
    deltaz = np.array([0., 0., delta, 0., 0., 0.])
    cpose = transformations.pose_euler_to_quaternion(cpose, deltaz, ee_rotation=True)
    arm.set_target_pose(pose=cpose, wait=wait, t=1.0)

def reach(wait=True):
    cpose = arm.end_effector()
    deltaz = np.array([0., 0., 0.01, 0., 0., 0.])
    cpose = transformations.pose_euler_to_quaternion(cpose, deltaz, ee_rotation=True)
    arm.set_target_pose(pose=cpose, wait=wait, t=1.0)
    rospy.loginfo(f"cposition = {arm.end_effector()[:3]}")
    cforce = arm.get_filtered_ft()
    rospy.loginfo(f"cforce = {cforce}")

    if cforce[2] < -1:
        rospy.logwarn("Collision")
        init_q = [1.5353, -1.211, -1.4186, -0.546, 1.6476, -0.0237]
        arm.set_joint_positions(position=init_q, wait=wait, t=1.0)


def main():
    parser = argparse.ArgumentParser(description='Test')
    parser.add_argument('-j', '--jnt', action='store_true',
                        help='read current joint angles')
    parser.add_argument('-e', '--eef', action='store_true',
                        help='read current eef pose')
    parser.add_argument('-f', '--force', action='store_true',
                        help='read current eef force')
    parser.add_argument('-m', '--move', action='store_true',
                        help='move to joint configuration')
    parser.add_argument('-mx', '--move_x',
                        nargs='?', const=0.04, type=float,
                        help='move the eef along its x-axis')
    parser.add_argument('-my', '--move_y',
                        nargs='?', const=0.04, type=float,
                        help='move the eef along its y-axis')
    parser.add_argument('-mz', '--move_z',
                        nargs='?', const=0.04, type=float,
                        help='move the eef along its z-axis')
    parser.add_argument('-r', '--reach',
                        nargs='?', const=1, type=int,
                        help='move the eef towards the table')
    args = parser.parse_args()
    rospy.init_node('ur5e_script_control_test')

    global arm
    arm = Arm(ft_sensor=True)

    real_start_time = timeit.default_timer()
    ros_start_time = rospy.get_time()

    if args.jnt:
        get_jnt()
    if args.eef:
        get_pose()
    if args.move:
        move_joints()
    if args.move_x:
        move_x(args.move_x)
    if args.move_y:
        move_y(args.move_y)
    if args.move_z:
        move_z(args.move_z)
    if args.reach:
        for _ in range(args.reach):
            reach()

    print("real time", round(timeit.default_timer() - real_start_time, 3))
    print("ros time", round(rospy.get_time() - ros_start_time, 3))


if __name__ == "__main__":
    main()
