# import matplotlib.pyplot as plt
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
import tensorflow as tf
# from tensorflow.keras.preprocessing import image_dataset_from_directory

BATCH_SIZE = 32
IMG_SIZE = (200, 200)
class FrameClassifier():

    def __init__(self, checkpoint_dir=None, model_path=None, labels=None):
        self.model = None
        self.checkpoint_dir = checkpoint_dir
        self.labels = labels
        self.model_path = model_path

        if model_path is not None:
            self.build_model()
        elif checkpoint_dir is not None:
            self.build_model_manually()
            self.load_weights()
        else:
            raise Exception("Needs to specify the checkpoint directory or the model directory")

    # Prepare model
    def build_model_manually(self):
        preprocess_input = tf.keras.applications.resnet_v2.preprocess_input

        # Create the base model from the pre-trained model MobileNet V2
        IMG_SHAPE = IMG_SIZE + (3,)
        base_model = tf.keras.applications.ResNet50V2(input_shape=IMG_SHAPE,
                                                    include_top=False,
                                                    weights='imagenet')

        # image_batch, label_batch = next(iter(train_dataset))
        # feature_batch = base_model(image_batch)

        global_average_layer = tf.keras.layers.GlobalAveragePooling2D()
        # feature_batch_average = global_average_layer(feature_batch)

        prediction_layer = tf.keras.layers.Dense(1)
        # prediction_batch = prediction_layer(feature_batch_average)

        inputs = tf.keras.Input(shape=(200, 200, 3))
        x = preprocess_input(inputs)
        x = base_model(x, training=False)
        x = global_average_layer(x)
        x = tf.keras.layers.Dropout(0.2)(x)
        outputs = prediction_layer(x)
        self.model = tf.keras.Model(inputs, outputs)

    def build_model(self):
        self.model = tf.keras.models.load_model(self.model_path)

    def load_weights(self):
        # Load weights
        latest = tf.train.latest_checkpoint(self.checkpoint_dir)
        print("Loading pretrained model:", latest)

        # Load the previously saved weights
        self.model.load_weights(latest)

    def load_image(self, image_path):
        if isinstance(image_path, str):
            img = tf.keras.preprocessing.image.load_img(
                image_path, target_size=IMG_SIZE
            )
        else:
            img = image_path # assume it is already loaded
        img_array = tf.keras.preprocessing.image.img_to_array(img)
        img_array = tf.expand_dims(img_array, 0) # Create a batch
        return img_array

    def predict(self, images, true_labels=None, verbose=False):
        img_array = None
        if isinstance(images, list):
            for image_path in images:
                if img_array is None:
                    img_array = self.load_image(image_path)
                else:
                    img_array = tf.concat([img_array, self.load_image(image_path)], axis=0)
        else:
            img_array = self.load_image(image_path)

        #Retrieve a batch of images from the test set
        predictions = self.model.predict(img_array).flatten()

        # Apply a sigmoid since our model returns logits
        predictions = tf.nn.sigmoid(predictions)
        predictions = tf.where(predictions < 0.5, 0, 1)

        if verbose:
            if self.labels is not None:
                print('Predictions:', [self.labels.get(pred) for pred in predictions.numpy()])
                if true_labels is not None:
                    print('True label:', self.labels.get(true_labels))
            else:
                print('Predictions:', predictions.numpy())

        return predictions.numpy()

# model_path = "/root/dev/image_classification/logs/training2.1-2001202113-29-37.h5"
# image_path = "/root/dev/image_classification/dataset/glue/01-4-093.png"
# image_path2 = "/root/dev/image_classification/ds-glue-no-glue/no-glue/01-1-001.png"
# # image_path = "/root/dev/image_classification/ds-glue-no-glue/no-glue/01-1-002.png"
# image_path3 = "/root/dev/image_classification/dataset/glue/01-4-095.png"
# labels = {0:"glue", 1:"no-glue"}

# # model_path = "/root/dev/image_classification/logs/training2.2-2001202112-10-11.h5"
# # image_path = "/root/dev/image_classification/ds-smooth/smooth/01-4-268.png"
# # image_path3 = "/root/dev/image_classification/ds-smooth/smooth/01-4-269.png"
# # image_path2 = "/root/dev/image_classification/ds-smooth/glue/01-4-093.png"
# # labels = {0:"glue", 1:"smooth"}

# p = FrameClassifier(model_path=model_path, labels=labels)
# p.predict([image_path, image_path2, image_path3], verbose=True)

