#!/usr/bin/python
#
# Send joint values to UR5 using messages
#

from trajectory_msgs.msg import JointTrajectory
from std_msgs.msg import Header
from trajectory_msgs.msg import JointTrajectoryPoint
from control_msgs.msg import JointTrajectoryControllerState # Used for subscribing to the UR.
import rospy

def _observation_callback(message):
    """This callback is set on the subscriber node in self.__init__().
    It's called by ROS every 40 ms while the subscriber is listening.
    Primarily updates the present and latest times.
    This callback is invoked asynchronously, so is effectively a
    "subscriber thread", separate from the control flow of the rest of
    GPS, which runs in the "main thread".
    message: observation from the robot to store each listen."""

    print(message)

def main():

    rospy.init_node('receive_joints')

    sub = rospy.Subscriber('/position_trajectory_controller/command',
                           JointTrajectory,
                           _observation_callback, queue_size=10)
    rospy.spin()

if __name__ == '__main__':
    main()
