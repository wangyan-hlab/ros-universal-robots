import numpy as np
from pyquaternion import Quaternion
np.set_printoptions(precision=3)
np.set_printoptions(suppress=True)
def motion_regression1D(sequence, t):
    """
        sequence list: list of poses and time stamps ((x0,t0),...,(xn,tn))
        t timestamp: target time
    """
    sx = 0.0
    stx = 0.0
    st2x = 0.0
    st = 0.0
    st2 = 0.0
    st3 = 0.0
    st4 = 0.0
    n = len(sequence[0])

    for i in range(n):
        x_i = sequence[0][i]
        t_i = sequence[1][i] - t
        sx += x_i
        stx += x_i * t_i
        st2x += x_i * t_i**2
        st += t_i
        st2 += t_i**2
        st3 += t_i**3
        st4 += t_i**4

    A = np.array([[st2, st, n], [st3, st2, st], [st4, st3, st2]])
    res = np.dot(np.linalg.inv(A), np.array([sx, stx, st2x]))
    return 2 * res[0], res[1]  # acc, vel


def motion_regression6D(sequence, t_seq, t):
    """
        sequence list: list of poses and time stamps ((p0,q0,t0),...,(pn,qn,tn))
        assumes q = [w,ax,ay,az]
        qt quaternion: 
        t timestamp: target time
    """
    n = len(sequence)

    vel = np.zeros(3)
    angular_vel = np.zeros(3)
    acc = np.zeros(3)
    angular_acc = np.zeros(3)
    dq = Quaternion()
    ddq = Quaternion()

    for i in range(7):
        seq = [sequence[:, i], t_seq]

        if i < 3: # position
            acc[i], vel[i] = motion_regression1D(seq, t)
        else: # rotation
            ddq[i-3], dq[i-3] = motion_regression1D(seq, t)
    # print (vel, acc)

    qt = Quaternion(sequence[-1,:][3:])
    vel = (qt.inverse * Quaternion(vector=vel) * qt).vector
    acc = (qt.inverse * Quaternion(vector=acc) * qt).vector
    angular_vel = (2 * dq * qt.inverse).vector
    angular_acc = (2 * ddq * qt.inverse).vector

    # account for fictitious forces
    acc -= np.cross(angular_vel, vel)
    return vel, acc, angular_vel, angular_acc


class MotionRegressor():
    def __init__(self, n, dt):
        """ 
            n int: lenght of sequence for regression 
        """
        self.seq = np.array([[0, 0, 0, 0, 0, 0, 0]] * n)
        self.dt = dt
        self.n = n

    def regress(self, p, q):
        pq = np.concatenate([p, q.elements]).reshape(1, -1)
        self.seq = np.delete(self.seq, -1, 0)
        self.seq = np.append(pq, self.seq, 0)
        reversed_seq = self.seq[::-1]
        return motion_regression6D(reversed_seq, self.create_time_seq(), self.dt*(self.n-1))

    def create_time_seq(self):
        seq = []
        for i in range(self.n):
            seq.append(self.dt*i)
        return np.array(seq)

    def reset(self):
        self.seq = np.array([[0, 0, 0, 0, 0, 0, 0]] * self.n)

class SimpleRegressor():
    def __init__(self, dt):
        self.dt = dt
        self.last_p = np.zeros(3)
        self.last_vel = np.zeros(3)
        self.last_q = Quaternion()
        self.last_dq = Quaternion()

    def regress(self, p, q):
        vel = (p - self.last_p) / self.dt
        acc = (vel - self.last_vel) / self.dt
        dq = (q - self.last_q) / self.dt
        ddq = (dq - self.last_dq) / self.dt

        ang_vel = (2 * q.inverse * dq).vector
        ang_acc = (2 * q.inverse * ddq).vector

        self.last_p = p
        self.last_vel = vel
        self.last_q = q
        self.last_dq = dq

        return vel, acc, ang_vel, ang_acc
    
    def reset(self):
        self.last_p = np.zeros(3)
        self.last_vel = np.zeros(3)
        self.last_q = Quaternion()
        self.last_dq = Quaternion()

ps = np.linspace(np.zeros(3), np.ones(3)*0.1, 10)
q1 = Quaternion([1, 0, 0, 0])
q2 = Quaternion([0.707, 0, 0, 0.707])
qs = Quaternion.intermediates(q1, q2, 10)
qs2 = Quaternion.intermediates(q1, q2, 10)
dt = 0.001

mr = MotionRegressor(4, dt)
sr = SimpleRegressor(dt)

for p, q in zip(ps, qs):
    print("Simple", sr.regress(p, q))

for p, q in zip(ps, qs2):
    print("Motion", mr.regress(p, q))
    
