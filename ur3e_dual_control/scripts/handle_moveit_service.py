#!/usr/bin/env python2
import argparse

from ur3e_dual_control.srv import ExecuteMoveItPlan, ExecuteMoveItPlanResponse
from dual_moveit_controller import DualMoveItControl, RIGHTARM, LEFTARM
import rospy

class MoveItService():
    def __init__(self, load_gripper=True, cautious=False, simulation=False):
        self.dual_controller = DualMoveItControl(load_gripper=load_gripper, cautious_mode=cautious, sim=simulation)
        self.plans_dict = {}

    def handle_moveit_planner(self, req):
        # Reset/load models into MoveIt planner
        pick_name = "pick_holder"
        spreader_name = "spreader_holder"
        if req.initialPose:
            # self.dual_controller.leftarm_gripper.open()
            self.dual_controller.remove_object(pick_name)
            self.dual_controller.detach_object(pick_name)
            self.dual_controller.remove_object(spreader_name)
            self.dual_controller.detach_object(spreader_name)
            self.dual_controller.remove_object("pipe")

            self.dual_controller.add_object(pick_name,     [1.055, 0.138, 1.032, 0, 0, 0], size=(0.065, 0.035, 0.035))
            self.dual_controller.add_object(spreader_name, [1.055, 0.235, 1.032, 0, 0, 0], size=(0.065, 0.035, 0.035))

        task_initial_pose = [2.0018, -1.6728, -1.142, -0.321, -0.4322, 0.0]
        task = [0.915, 0.018, 1.059, 0.5, -0.5, 0.5, 0.5]
        rightarm_init_pose = [0.78, -0.075, 1.042, -0.500, 0.500, -0.500, 0.500]
        right_posture = [-1.1559, -0.7517, 1.7833, -2.603, 1.5705, -1.9794]

        if req.initialPose:
            print ">>> Go to Initial Positions"
            self.dual_controller.go_to_joint_state(task_initial_pose)
            # self.dual_controller.go_to_pose_goal(task, arm=LEFTARM)
            self.dual_controller.go_to_joint_state(right_posture, arm=RIGHTARM)
            # self.dual_controller.go_to_pose_goal(rightarm_init_pose, arm=RIGHTARM)
            # self.dual_controller.add_object(name="pipe", pose=[0.73, -0.10, 1.06, 0, 1.5707, 0], type="cylinder", size=[0.3, 0.01])

        ### Pick Holder Seq ###
        pre_grasp_pick_holder_posture = [1.339, -1.7301, -1.6825, -1.2994, -1.5711, 0.2309]
        grasp_pick_holder_pose = [1.040, 0.141, 1.0255, 0.500, 0.500, 0.500, 0.500]
        
        if req.graspPick:
            print ">>> Pick Pick-Holder"
            # 1. go from initial task posture to support (pre_grasp_posture)
            self.dual_controller.go_to_joint_state(pre_grasp_pick_holder_posture, save_as="task_to_pick")
            self.dual_controller.pick_place_tool(pre_grasp_posture=pre_grasp_pick_holder_posture, 
                                            grasp_pose=grasp_pick_holder_pose,
                                            pick=True, tool_name=pick_name)
            self.dual_controller.go_to_joint_state(task_initial_pose, save_as="pick_to_task")
                                    

        if req.placePick:
            print ">>> Place Pick-Holder"
            # 1. go from initial task posture to support (pre_grasp_posture)
            self.dual_controller.go_to_joint_state(task_initial_pose)
            self.dual_controller.go_to_joint_state(pre_grasp_pick_holder_posture, save_as="task_to_pick")
            self.dual_controller.pick_place_tool(pre_grasp_posture=pre_grasp_pick_holder_posture, 
                                            grasp_pose=grasp_pick_holder_pose,
                                            pick=False, tool_name=pick_name)
            ### Pick Seq END ###

        ### Spreader Holder Seq ###
        # pre_grasp_spreader_holder_posture = [1.6446, -1.538, -1.8893, -1.2841, -1.5706, -0.0739]
        # grasp_spreader_holder_pose = [1.047, 0.235, 1.0312, 0.500, 0.500, 0.500, 0.500]
        pre_grasp_spreader_holder_posture = [1.339, -1.7301, -1.6825, -1.2994, -1.5711, 0.2309]
        grasp_spreader_holder_pose = [1.040, 0.141, 1.0255, 0.500, 0.500, 0.500, 0.500]

        if req.graspSpreader:
            print ">>> From Pick to Spreader Holder"
            self.dual_controller.go_to_joint_state(pre_grasp_spreader_holder_posture, save_as="pick_to_spreader")
            self.dual_controller.pick_place_tool(pre_grasp_posture=pre_grasp_spreader_holder_posture, 
                                            grasp_pose=grasp_spreader_holder_pose,
                                            pick=True, tool_name=spreader_name)
            self.dual_controller.go_to_joint_state(task_initial_pose, save_as="spreader_to_task")


        if req.placeSpreader:
            print ">>> Place Spreader-Holder"
            self.dual_controller.go_to_joint_state(task_initial_pose)
            self.dual_controller.go_to_joint_state(pre_grasp_spreader_holder_posture, save_as="task_to_spreader")
            self.dual_controller.pick_place_tool(pre_grasp_posture=pre_grasp_spreader_holder_posture, 
                                            grasp_pose=grasp_spreader_holder_pose,
                                            pick=False, tool_name=spreader_name)

        return True

    def moveit_planner_server(self):
        s = rospy.Service('moveit_ur3e_dual', ExecuteMoveItPlan, self.handle_moveit_planner)
        print("Ready to receive petitions")
        rospy.spin()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Test moveit dual control')
    parser.add_argument('--sim', action='store_true', help='for the simulation')

    args = parser.parse_args()

    serv = MoveItService(simulation=args.sim, cautious=False)
    serv.moveit_planner_server()