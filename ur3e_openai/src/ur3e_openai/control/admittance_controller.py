import sys
import rospy
import numpy as np

from gym import spaces

from pyquaternion import Quaternion
from ur_control import utils, transformations, spalg
from ur_control.admittance_control import AdmittanceModel
from ur3e_openai.control import controller
from ur3e_openai.robot_envs.utils import get_value_from_range


class AdmittanceController(controller.Controller):
    def __init__(self, arm, agent_control_dt):
        controller.Controller.__init__(self, arm, agent_control_dt)
        self.force_control_model = self._init_admittance_controller()

    def _init_admittance_controller(self):
        position_kp = self.base_position_kp
        position_kd = np.array(position_kp) * 0.1

        M = self.inertia_const * np.ones(6)
        K = self.stiffness_const * np.ones(6)
        B = 2*self.damping_ratio*np.sqrt(M*K)

        model = AdmittanceModel(M, K, B, self.robot_control_dt, method=self.impedance_method)
        model.p_controller = utils.PID(Kp=position_kp, Kd=position_kd)

        return model

    def reset(self):
        self.force_control_model.p_controller.reset()

    def act(self, action, target):
        assert action.shape == (self.n_actions, )
        if np.any(np.isnan(action)):
            rospy.logerr("Invalid NAN action(s)" + str(action))
            sys.exit()
        assert np.all(action >= -1.0001) and np.all(action <= 1.0001)

        # ensure that we don't change the action outside of this scope
        actions = np.copy(action)

        actions_imp = None
        actions_pd = None

        if self.n_actions == 8:
            actions_pd = [actions[6]]
            actions_imp = actions[7:]
        elif self.n_actions == 13 and not self.pd_dofs_is_6:
            actions_pd = [actions[6]]
            actions_imp = actions[7:]
        elif self.n_actions == 13 and self.pd_dofs_is_6:
            actions_pd = actions[6:12]
            actions_imp = actions[12:]
        elif self.n_actions == 18:
            actions_pd = actions[6:12]
            actions_imp = actions[12:]
        else:
            raise Exception("action space not supported", self.n_actions)

        self.set_impedance_parameters(actions_imp)
        self.set_pd_parameters(actions_pd)

        position_action = self.set_position_signal(actions[:6])

        # Take action
        action_result = self.ur3e_arm.set_impedance_control(
            target_pose=target,
            target_force=self.desired_force_torque,
            model=self.force_control_model,
            timeout=self.agent_control_dt,
            max_force_torque=self.max_force_torque,
            action=position_action
        )
        return action_result

    def set_position_signal(self, actions):
        action = [np.interp(actions[i], [-1, 1], [-1*self.max_speed[i], self.max_speed[i]]) for i in range(len(actions))]
        action /= self.action_scale

        return action

    def set_pd_parameters(self, action):
        if len(action) == 1:
            position_kp = [get_value_from_range(action, self.base_position_kp[0], self.kpd_range)]*6
        else:
            assert len(action) == 6
            position_kp = [get_value_from_range(act, self.base_position_kp[i], self.kpd_range, mtype=self.pd_range_type) for i, act in enumerate(action[:6])]
            
        position_kd = np.array(position_kp) * 0.1
        self.force_control_model.p_controller.set_gains(Kp=position_kp, Kd=position_kd)

    def set_impedance_parameters(self, action):
        if isinstance(action, list):
            K = [get_value_from_range(action[i], self.stiffness_const[i], self.param_range) for i in range(6)]
            K = np.array(K)
            M = self.inertia_const * np.ones(6)
            B = 2*self.damping_ratio*np.sqrt(M*K)
            self.force_control_model.set_constants(M, K, B, self.robot_control_dt)
        else:
            K = get_value_from_range(action, self.stiffness_const[0], self.param_range)
            K = K * np.ones(6)
            M = self.inertia_const * np.ones(6)
            B = 2*self.damping_ratio*np.sqrt(M*K)
            self.force_control_model.set_constants(M, K, B, self.robot_control_dt)
