"""
	Description: All you need to build a robot simulation environment with ROS (Noetic) + Gazebo 11 
	Version: Python 3 version
	Author: Wang, Yan
	Usage: (Not tested yet) 'source' this file to build your environment
"""

# Setup timezone
echo 'Etc/UTC' > /etc/timezone && \
ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
apt-get update && apt-get install -q -y tzdata

# Setup sources.list
echo "deb http://packages.ros.org/ros/ubuntu bionic main" > /etc/apt/sources.list.d/ros1-latest.list

# Setup keys
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

# Install bootstrp tools
apt-get update && apt-get install --no-install-recommends -y \
	python3-rosdep \
	python3-rosinstall \
	python3-vcstools    

# Bootstrap rosdep
rosdep init \
&& rosdep update

# Install ros packages
apt-get update && apt-get install -y ros-noetic-ros-core
apt-get install -y ros-noetic-ros-base

# Install Universal Robot ros packages
apt-get install -y \
    	ros-$ROS_DISTRO-gazebo-ros-pkgs \
    	ros-$ROS_DISTRO-spacenav-node \
    	ros-$ROS_DISTRO-rqt-common-plugins \
    	# install catkin
    	ros-$ROS_DISTRO-catkin \
    	python-catkin-tools \
    	# Install Numpy Boost
    	libboost-dev \
    	libboost-python-dev \
    	libboost-system-dev \
    	libboost-all-dev \
    	libatlas-base-dev \
    	libprotobuf-dev \
   	protobuf-compiler \
    	# python dependencies
   	python-setuptools \
    	python3-tk \
    	python3-numpy \
    	# utils
    	locate \
    	aptitude \
    	vim htop tmux \
    	curl wget \
    	tk \
    	spacenavd	

# Python libraries
python -m pip install pip --upgrade && \
pip install matplotlib==2.2.3 spicy protobuf pyyaml pyquaternion rospkg \
    	lxml tqdm catkin-pkg empy PyVirtualDisplay defusedxml gym psutil pyprind

# Gazebo
## Setup keys
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys D2486D2DD83DB69272AFE98867170598AF249743

## Setup sources.list
. /etc/os-release \
&& echo "deb http://packages.osrfoundation.org/gazebo/$ID-stable `lsb_release -sc` main" > /etc/apt/sources.list.d/gazebo-latest.list

## Install gazebo packages
apt-get update && apt-get install -q -y \
    	binutils \
    	mesa-utils \
    	module-init-tools \
    	x-window-system \
    	gazebo11 \
    	libgazebo11-dev

# Decompress two packages
tar -xvf orocos_kinematics_dynamics.tar.xz -C ~/
tar -zxvf sip-4.19.8.tar.gz -C ~/

# Install sip-4.19.8
cd ~/sip-4.19.8 \
&& python configure.py \
&& make -j4 && make install

# Install PyKDL
apt update && apt -y install libeigen3-dev
cd ~/orocos_kinematics_dynamics/orocos_kdl \
&& mkdir build && cd build \
&& cmake -DCMAKE_BUILD_TYPE=Release .. \
&& make -j4 && make install

cd ~/orocos_kinematics_dynamics/python_orocos_kdl \
&& mkdir build && cd build \
&& cmake -DCMAKE_BUILD_TYPE=Release -DPYTHON_INCLUDE_DIR=$(python -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())")  -DPYTHON_LIBRARY=$(python -c "import distutils.sysconfig as sysconfig; print(sysconfig.get_config_var('LIBDIR'))") -DPYTHON_VERSION=3 .. \
&& make -j4

# ROS workspace
# Setup the workspace
source /opt/ros/$ROS_DISTRO/setup.bash \
&& mkdir -p ~/ros_ws/src \
&& cd ~/ros_ws/ \
&& catkin init

# Installing repo
cd ~/ros_ws/src \
&& git clone https://gitlab.com/wangyan-hlab/ros-universal-robots.git

# Updating rosdep and installing dependencies
cd ~/ros_ws \
&& rosinstall ~/ros_ws/src /opt/ros/$ROS_DISTRO src/ros-universal-robots/dependencies.rosinstall \
&& apt update -qq \
&& rosdep fix-permissions \
&& rosdep update -q \
&& rosdep install --from-paths src --ignore-src --rosdistro=$ROS_DISTRO -y

# Compiling ROS workspace
source /opt/ros/$ROS_DISTRO/setup.bash \
&& cd ~/ros_ws/ \
&& rm -rf build \
&& catkin build -DPYTHON_EXECUTABLE=/usr/bin/python3 -DPYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.8m.so -DPYTHON_VERSION=3

# Adding rqt-multiplot
apt-get update && apt-get install -y ros-$ROS_DISTRO-rqt-multiplot

# Installing other python libraries
pip install xmltodict imageio IPython
pip install PySide2 PyQt5 transform3d

# Compiling ROS workspace
source /opt/ros/$ROS_DISTRO/setup.bash \
&& cd ~/ros_ws/ \
&& rm -rf build \
&& catkin build -DPYTHON_EXECUTABLE=/usr/bin/python3 -DPYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.8.so -DPYTHON_VERSION=3

pip3 install pyserial

# Custom python libs
pip install Cython cpprb

# ur_ikfast
cd ~/ \
&& mkdir pylibs && cd pylibs \
&& git clone https://github.com/cambel/ur_ikfast.git \
&& cd ur_ikfast && pip install -e .

apt install -y python-pip
python2 -m pip install pyquaternion scipy Cython

# ur_ikfast python2
cd ~/pylibs/ur_ikfast &&  python2 -m pip install -e .

# fix for sip
rm /usr/lib/python3/dist-packages/sip.cpython-36m-x86_64-linux-gnu.so
# fix gazebo
apt update && apt-mark hold cuda-compat-10-1 libnvinfer-plugin6 libnvinfer6 libcudnn7 && apt upgrade -y

pip install future netifaces pandas sklearn seaborn

# clean
pip cache purge
rm -rf /var/lib/apt/lists/*

# setup environment
echo 'source /opt/ros/$ROS_DISTRO/setup.bash' >> ~/.bashrc
echo 'source ~/ros_ws/devel/setup.bash' >> ~/.bashrc

echo 'source /usr/share/gazebo/setup.sh' >> ~/.bashrc
echo 'export PYTHONPATH=/root/orocos_kinematics_dynamics/python_orocos_kdl/build:/root/gps:$PYTHONPATH' >> ~/.bashrc
echo 'export GAZEBO_RESOURCE_PATH=$GAZEBO_RESOURCE_PATH:~/ros_ws/src/ros-universal-robots/ur3_gazebo/models/' >> ~/.bashrc

