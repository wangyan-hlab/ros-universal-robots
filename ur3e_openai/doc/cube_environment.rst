Cube Single Disk Environment
============================

The **SingleDisk Cube Environment** is based on the **Cubi Robot** created at **ETH Zurich**.
Its a simpler version with only one inertial disk instead of the **three** of the original.

.. figure:: img/moving_cube_image.png
   :scale: 100 %
   :alt: Single Disk Cube Image

   Single Disk Cube in ROSDS.

Robot Environment
*****************

ur3e_openai.robot_envs.cube_single_disk_env module
-------------------------------------------------

.. automodule:: ur3e_openai.robot_envs.cube_single_disk_env
    :members:
    :undoc-members:
    :show-inheritance:


Task Environments
*****************

.. toctree::
   :maxdepth: 4

   ur3e_openai.task_envs.moving_cube
