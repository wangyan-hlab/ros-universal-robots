import numpy as np
from frame_classifier import FrameClassifier
import glob

class PipeClassifier():
    def __init__(self, tolerance=0.9):
        """
           tolerance (float): percentage where the prediction is acceptable
        """
        self.glue_classifier = None
        self.smooth_classifier = None
        self.tolerance = tolerance

    def load_model_glue(self, checkpoint_dir=None, model_path=None):
        self.glue_classifier = FrameClassifier(checkpoint_dir, model_path)

    def load_model_smooth(self, checkpoint_dir=None, model_path=None):
        self.smooth_classifier = FrameClassifier(checkpoint_dir, model_path)

    def _predict(self, images, model, target_label):
        predictions = model.predict(images, verbose=True)
        # We assume that the target label is 1, there are just two labels [0,1]
        perc = np.average(predictions) if target_label == 1 else 1.0 - np.average(predictions)
        print("percent", perc)
        return 1 if perc > self.tolerance else 0


    def predict_glue(self, images):
        if self.glue_classifier is not None:
            return self._predict(images, self.glue_classifier, target_label=0)
        else:
            raise Exception("Classifier not initialized")

    def predict_smooth(self, images):
        if self.smooth_classifier is not None:
            return self._predict(images, self.smooth_classifier, target_label=1)
        else:
            raise Exception("Classifier not initialized")


p = PipeClassifier()

glue_model_path = "/root/dev/image_classification/logs/training2.1-2001202113-29-37.h5"
p.load_model_glue(model_path=glue_model_path)

smooth_model_path = "/root/dev/image_classification/logs/training2.2-2001202112-10-11.h5"
p.load_model_smooth(model_path=smooth_model_path)

image_list = sorted(glob.glob('/root/dev/image_classification/pipe_seq_test/03/*.png'))
print(p.predict_glue(image_list))
import timeit
start_time = timeit.default_timer()
image_list = sorted(glob.glob('/root/dev/image_classification/pipe_seq_test/07/*.png'))
print(p.predict_smooth(image_list))
print(timeit.default_timer()-start_time)
